const path = require('path');

const {
  everyDayGetPacket,
  collectCard,
  sharePage,
    article,
    coupon,
    ownGiftCard,
    jingBean,
    feedback,
    download,
    msgCenter,
    orderPage,
    privacy,
  intelligence
} = require('./config/index.js');
let data = {};
if (process.env.VUE_APP_TITLE === 'every-day-get-packet') {
  data = everyDayGetPacket
} else if (process.env.VUE_APP_TITLE === 'collect-card') {
  data = collectCard
} else if (process.env.VUE_APP_TITLE === 'share-page') {
  data = sharePage
} else if (process.env.VUE_APP_TITLE === 'article') {
  data = article
} else if (process.env.VUE_APP_TITLE === 'coupon') {
  data = coupon
} else if (process.env.VUE_APP_TITLE === 'own-gift-card') {
  data = ownGiftCard
} else if (process.env.VUE_APP_TITLE === 'jing-bean') {
  data = jingBean
}else if (process.env.VUE_APP_TITLE === 'feedback') {
  data = feedback
}else if (process.env.VUE_APP_TITLE === 'download') {
  data = download
}else if (process.env.VUE_APP_TITLE === 'msgCenter') {
  data = msgCenter
}else if (process.env.VUE_APP_TITLE === 'orderPage') {
  data = orderPage
}else if (process.env.VUE_APP_TITLE === 'privacy') {
  data = privacy
}else if (process.env.VUE_APP_TITLE === 'intelligence') {
  data = intelligence
}

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '././' : '/',
  productionSourceMap: process.env.NODE_ENV !== 'production',
  outputDir: data.outputDir,
  lintOnSave: false,
  configureWebpack: {
    resolve: {
      alias: data.alias
    }
  },
  devServer: {
    disableHostCheck: true,
    proxy: {
      '/api': {
        target: 'http://hlc.jd.com:8080'
      }
    },
    before(app) {
      app.get('/api', (req, res) => {
        let functionId = req.query.functionId;
        let path = `./src/mock/every-day-get-packet/${functionId}.json`;
        res.json(require(path))
      })
    }
  },
  pages: {
    index: {
      entry: data.entry,
      template: 'public/index.html'
    }
  }
}
