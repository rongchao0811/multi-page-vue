const everyDayGetPacket = {
	outputDir: 'dist/every-day-get-packet',
	entry: 'src/views/every-day-get-packet/index.js'
}

module.exports = {
	publicPath: '/',
	outputDir: 'dist',
	assetsDir: '',
	indexPath: 'index.html'
}
