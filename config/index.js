const everyDayGetPacket = {
	alias: {
		'images': '@/assets/images/every-day-get-packet'
	},
	outputDir: 'dist/every-day-get-packet',
	entry: 'src/views/every-day-get-packet/index.js'
}

const collectCard = {
	alias: {
		'images': '@/assets/images/collect-card'
	},
	outputDir: 'dist/collect-card',
	entry: 'src/views/collect-card/index.js'
}

const article = {
	alias: {
		'images': '@/assets/images/article'
	},
	outputDir: 'dist/article',
	entry: 'src/views/article/index.js'
}
const coupon = {
	alias: {
		'images': '@/assets/images/coupon'
	},
	outputDir: 'dist/coupon',
	entry: 'src/views/coupon/index.js'
}
const sharePage = {
	alias: {
		'images': '@/assets/images/share-page'
	},
	outputDir: 'dist/share-page',
	entry: 'src/views/share-page/index.js'
}
const ownGiftCard = {
	alias: {
		'images': '@/assets/images/own-gift-card'
	},
	outputDir: 'dist/own-gift-card',
	entry: 'src/views/own-gift-card/index.js'
}
const jingBean = {
	alias: {
		'images': '@/assets/images/jing-bean'
	},
	outputDir: 'dist/jing-bean',
	entry: 'src/views/jing-bean/index.js'
}
const feedback = {
	alias: {
		'images': '@/assets/images/feedback'
	},
	outputDir: 'dist/feedback',
	entry: 'src/views/feedback/index.js'
}
const download = {
	alias: {
		'images': '@/assets/images/feedback'
	},
	outputDir: 'dist/download',
	entry: 'src/views/download/index.js'
}
const msgCenter = {
	alias: {
		'images': '@/assets/images/msgCenter'
	},
	outputDir: 'dist/msgCenter',
	entry: 'src/views/msgCenter/index.js'
}
const orderPage = {
	alias: {
		'images': '@/assets/images/msgCenter'
	},
	outputDir: 'dist/orderPage',
	entry: 'src/views/orderPage/index.js'
}
const privacy = {
	alias: {
		'images': '@/assets/images/privacy'
	},
	outputDir: 'dist/privacy',
	entry: 'src/views/privacy/index.js'
}
const intelligence = {
	alias: {
		'images': '@/assets/images/intelligence'
	},
	outputDir: 'dist/intelligence',
	entry: 'src/views/intelligence/index.js'
}
module.exports = {
	everyDayGetPacket,
	collectCard,
	sharePage,
	article,
	coupon,
	ownGiftCard,
	jingBean,
	feedback,
	download,
	msgCenter,
	orderPage,
	privacy,
	intelligence
}
