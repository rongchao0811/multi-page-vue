import Vue from 'vue'
import App from './index.vue'
import router from '@/router/collect-card'

import '@/assets/css/common/reset.css'
import '@/utils/rem.js'

Vue.config.productionTip = false

router.afterEach(to => {
  document.title = to.meta.title;
});

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')