/**
 * @param {
 *  prizeCount:      [Number] 奖品个数
 *  currentIndex:    [Number] 当前停留位置
 *  speed:           [Number] 初始速度
 *  baseCount:       [Number] 基础转动次数
 *  runningCallback: [Function] 转动过程中，每转动1次调用回调函数
 *  baseCount:       [Function] 抽奖成功回调函数
 * }
 */
export class NineGridLottery {
  constructor({
    prizeCount      = 8,
    currentIndex    = -1,
    speed           = 100,
    baseCount       = 50,
    runningCallback = () => {console.log('running')},
    successCallback = () => { console.log('lottery success !') }
  } = {}) {
    this.baseCount       = baseCount;
    this.currentIndex    = currentIndex;
    this.prizeCount      = prizeCount;
    this.speed           = speed;
    this.count           = -1;
    this.timer           = 0;
    this.runningCallback = runningCallback;
    this.successCallback = successCallback;
  }

  lottery(prizeIndex) {
    this.prizeIndex = prizeIndex;
    this.reset();
    this.running();
  }

  reset() {
    clearTimeout(this.timer);
    this.timer = 0;
    this.speed = 100;
    this.count = this.currentIndex;
  }

  running() {
    this.count++;
    this.currentIndex = this.count % this.prizeCount;

    this.runningCallback(this.currentIndex);

    if (this.count > (this.baseCount + 10) && this.prizeIndex === this.currentIndex) {
      // 抽奖完成
      clearTimeout(this.timer);
      setTimeout(() => {
        this.successCallback();
      }, 500);
    } else {
      if (this.count < this.baseCount) {
        this.speed -= 10;
      } else {
        if (this.count > (this.baseCount + 10) && ((this.prizeIndex === 0 && this.currentIndex == (this.prizeCount - 1)) || this.prizeIndex === (this.currentIndex + 1))) {
          // 当下一个位置为中奖位置时，速度瞬间大幅度减慢
          this.speed += 200;
        } else {
          this.speed += 10;
        }
      }
      if (this.speed < 40) {
        this.speed = 40;
      }
      this.timer = setTimeout(() => {
        this.running();
      }, this.speed);
    }
  }
}
