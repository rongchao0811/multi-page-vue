import Vue from 'vue'
import App from './index.vue'

import '@/utils/lib/wx-js-sdk.js'
import '@/utils/toast.js'
import '@/utils/rem.js'
import '@/assets/css/common/reset.css'
import router from '@/router/msgCenter'

import { EventBus } from '@/utils/event-bus.js'
import GlobalComponent from "@/utils/components/index.js"
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import '@/assets/css/common/reset.css'
import '@/assets/css/common/coupon.css'
Vue.use(MintUI)

Vue.use(GlobalComponent);

Vue.config.productionTip = false;
Vue.prototype.EventBus = EventBus;

window.toast = new ToastClass();
window.salert = new AlertClass();

Vue.filter('number2Decimal', function (val) {
  val = Number(val);
  if (isNaN(val)) {
    val = 0;
  }
  return val.toFixed(2);
});

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
