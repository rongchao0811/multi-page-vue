import Vue from 'vue'
import App from './index.vue'

import '@/utils/lib/wx-js-sdk.js'
import '@/utils/toast.js'
import '@/utils/rem.js'
import '@/assets/css/common/reset.css'

import { EventBus } from '@/utils/event-bus.js'
import GlobalComponent from "@/utils/components/index.js"

Vue.use(GlobalComponent);

Vue.config.productionTip = false;
Vue.prototype.EventBus = EventBus;

window.toast = new ToastClass();
window.salert = new AlertClass();

Vue.filter('number2Decimal', function (val) {
  val = Number(val);
  if (isNaN(val)) {
    val = 0;
  }
  return val.toFixed(2);
});

new Vue({
  render: h => h(App),
}).$mount('#app')
