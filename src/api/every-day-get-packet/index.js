import request from '@/utils/request.js'
import { jdLogin } from '@/utils/index.js'

const showErrorMessage = function(msg = '活动太火爆，请稍后再试！') {
  toast.show({
    text: msg,
    duration: 2000
  })
}

/**
 * 获取活动列表
 * 入参：systemId
 * 出参（主要字段）：
 *  1. activityId 2. activityType 3.extData
 */
export function requestGetActivityInfo(params) {
  let functionId = 'jdh_queryActivity';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 获取首页推荐商品信息
 * 入参：systemId
 * 出参（主要字段）：
 *  1. activityId 2. activityType 3.extData
 */
export function requestGetProductInfo(params) {
  let functionId = 'jdh_queryHomeRecommendInfo';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 创建用户活动信息
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. activityType
 * 出参（主要字段）：
 *  1. userActivityId
 *  2. wxName
 *  3. wxImage
 */
export function requestToCreateUserActivityInfo(params) {
  let functionId = 'jdh_startBoost';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else if (code == 999) {
        jdLogin();
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 获取用户活动信息
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. userActivityId
 * 出参（主要字段）：
 *  1. joyData
 */
export function requestGetUserActivityInfo(params) {
  let functionId = 'jdh_queryUserActivity';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else if (code == 3001 || code == 2004) {
        showErrorMessage(res.message);
      } else {
        showErrorMessage();
      }

    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 抽奖接口
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. userActivityId
 * 4. index
 * 出参（主要字段）：
 * 1. readBagPrice
 */
export function requestToLuckDraw(params) {
  let functionId = 'jdh_luckDraw';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0 || code == 4005) {
        resolve(result);
      } else  {
        showErrorMessage();
      }

    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 帮助助力
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. userActivityId
 * 出参（主要字段）：
 * 1. joyData
 * 2. extData
 */
export function requestToHelpBoost(params) {
  let functionId = 'jdh_helpBoost';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 999) {
        jdLogin();
      } else {
        if ( code == 2001 || code == 2003 || code == 2004 || code == 3001 || code == 4001) {
          showErrorMessage(res.message);
        } else if (code !== 0 && code !== 2002) {
          showErrorMessage();
        }
        resolve(res);
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 获取助力记录列表
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. userActivityId
 * 出参（主要字段）：
 * 1. result
 */
export function requestGetBoostRecordList(params) {
  let functionId = 'jdh_queryHelpBoostList';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 获红包参与记录列表
 * 入参：
 * 1. systemId
 * 2. activityId
 * 3. userActivityId
 * 出参（主要字段）：
 * 1. result
 */
export function requestGetPartakeRecordList(params) {
  let functionId = 'jdh_queryUserActivityList';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

/**
 * 获红包“我的参与”记录列表
 * 入参：
 * 1. systemId
 * 2. hbState
 * 出参（主要字段）：
 * 1. result
 */
export function requestGetAccumulatedPacketRecord(params) {
  let functionId = 'jdh_queryLotteryRecordList';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.result || {};

      if (code == 0) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}
