import request from '@/utils/request1.js'

const showErrorMessage = function(msg = '活动太火爆，请稍后再试！') {
  toast.show({
    text: msg,
    duration: 2000
  })
}

/**
 * 传递设备信息及剪切板信息
 * 入参：systemId
 * 出参（主要字段）：
 *  1. activityId 2. activityType 3.extData
 */
export function requestPostActivityInfo(params) {
  let functionId = 'jdh_ic_addInviteInfo';
  return new Promise(resolve => {
    request({
      url: `/api?functionId=${functionId}`,
      data: params
    }).then(res => {
      let code   = res.code;
      let result = res.data || {};
      if (code == 200) {
        resolve(result);
      } else {
        showErrorMessage();
      }
    }).catch(error => {
      console.error(`functionId: ${functionId}, error:${error}`)
    })
  })
}

