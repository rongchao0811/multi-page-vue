import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const Home    = () => import('@/views/collect-card/Home');     // 首页
const PumpCard = () => import('@/views/collect-card/PumpCard'); // 抽卡
const SignIn   = () => import('@/views/collect-card/SignIn');   // 签到
const Answer   = () => import('@/views/collect-card/Answer');   // 答题
const StartAnswer   = () => import('@/views/collect-card/Answer/StartAnswer');   // 开始答题
const Checkpoint   = () => import('@/views/collect-card/Answer/Checkpoint');   // 闯关模式
const Challenge   = () => import('@/views/collect-card/Answer/Challenge');   // 挑战模式
const LuckDraw = () => import('@/views/collect-card/LuckDraw'); // 抽奖
const MineField = () => import('@/views/collect-card/MineField'); // 矿场
const Shop = () => import('@/views/collect-card/Shop'); // 商店
const Repair = () => import('@/views/collect-card/Repair'); // 维修铺
const Business = () => import('@/views/collect-card/Business'); // 交易
const Compose = () => import('@/views/collect-card/Compose'); // 分解
const Decompose = () => import('@/views/collect-card/Decompose'); // 分解
const RankList = () => import('@/views/collect-card/RankList'); // 排行榜
const GameRule = () => import('@/views/collect-card/GameRule'); // 规则
const NoticeList = () => import('@/views/collect-card/NoticeList'); // 公告
const Handbook = () => import('@/views/collect-card/Handbook'); // 图鉴
const Personal = () => import('@/views/collect-card/Personal'); // 我的
const CardBag = () => import('@/views/collect-card/Personal/CardBag'); // 我的卡包
const Shelves = () => import('@/views/collect-card/Personal/Shelves'); // 我的货架


export default new VueRouter ({
  base: '/collect-card',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      meta: { name: '首页', title: '集卡大作战' },
      component: Home
    },
    {
      path: '/pumpCard',
      name: 'pump-card',
      meta: { name: '集卡', title: '幸运抽取卡片' },
      component: PumpCard
    },
    {
      path: '/sign-in',
      name: 'sign-in',
      meta: { name: '签到', title: '签到赚金币' },
      component: SignIn
    },
    {
      path: '/answer',
      name: 'answer',
      meta: { name: '答题', title: '每日答题' },
      component: Answer
    },
    {
      path: '/answer/start-answer',
      name: 'start-answer',
      meta: { type: 'children', name: '开始答题', title: '开始答题' },
      component: StartAnswer
    },
    {
      path: '/answer/challenge',
      name: 'challenge',
      meta: { type: 'children', name: '挑战模式', title: '挑战模式' },
      component: Challenge
    },
    {
      path: '/answer/checkpoint',
      name: 'checkpoint',
      meta: { type: 'children', name: '闯关模式', title: '闯关模式' },
      component: Checkpoint
    },
    {
      path: '/luck-draw',
      name: 'luck-draw',
      meta: { name: '抽奖', title: '每日免费抽奖' },
      component: LuckDraw
    },
    {
      path: '/mine-field',
      name: 'mine-field',
      meta: { name: '矿场', title: '矿场' },
      component: MineField
    },
    {
      path: '/shop',
      name: 'shop',
      meta: { name: '商店', title: '商店' },
      component: Shop
    },
    {
      path: '/repair',
      name: 'repair',
      meta: { name: '维修铺', title: '维修铺' },
      component: Repair
    },
    {
      path: '/business',
      name: 'business',
      meta: { name: '交易', title: '交易市场' },
      component: Business
    },
    {
      path: '/compose',
      name: 'compose',
      meta: { name: '合成', title: '合成' },
      component: Compose
    },
    {
      path: '/decompose',
      name: 'decompose',
      meta: { name: '分解', title: '分解' },
      component: Decompose
    },
    {
      path: '/rank-list',
      name: 'rank-list',
      meta: { name: '排行榜', title: '好友卡片收集排行榜' },
      component: RankList
    },
    {
      path: '/rule',
      name: 'rule',
      meta: { name: '规则', title: '游戏规则' },
      component: GameRule
    },
    {
      path: '/notice-list',
      name: 'notice-list',
      meta: { name: '公告', title: '活动公告' },
      component: NoticeList
    },
    {
      path: '/handbook',
      name: 'handbook',
      meta: { name: '图鉴', title: '卡片介绍' },
      component: Handbook
    },
    {
      path: '/personal',
      name: 'personal',
      meta: { name: '我的', title: '个人中心' },
      component: Personal
    },
    {
      path: '/personal/card-bag',
      name: 'personal-card-bag',
      meta: { type: 'children', name: '我的卡包', title: '我的卡包' },
      component: CardBag
    },
    {
      path: '/personal/shelves',
      name: 'personal-shelves',
      meta: { type: 'children', name: '我的货架', title: '我的货架' },
      component: Shelves
    },
  ]
})