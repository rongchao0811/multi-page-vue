import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter ({
  base: '/',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      meta: { name: '用户反馈', title: '用户反馈' },
      component: () => import('@/views/feedback/index')
    },
  ]
})
