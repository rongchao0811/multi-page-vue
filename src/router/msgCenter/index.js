import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter ({
  base: '/',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      meta: { name: '消息中心', title: '消息中心' },
      component: () => import('@/views/msgCenter/index')
    },
  ]
})
