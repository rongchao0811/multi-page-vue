import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter ({
  base: '/coupon',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      meta: { name: '优惠券', title: '优惠券' },
      component: () => import('@/views/coupon/index')
    },
  ]
})
