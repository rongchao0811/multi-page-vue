import axios from 'axios'

// const baseURL = 'http://hlc.jd.com:8080';
// const baseURL = 'https://beta-api.m.jd.com';
const baseURL = 'https://api.m.jd.com';

const service = axios.create({
  baseURL: baseURL, // 接口地址
  timeout: 6000, // 请求超时时间
  withCredentials: true
})
// service.defaults.headers.post['Content-Type'] = 'multipart/form-data'
// 请求前
service.interceptors.request.use(
  config => {
    toast.show({text: '', loading: true});
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// respone拦截器
service.interceptors.response.use(
  response => {
    const res = response.data;
    toast.hide();
    return res;
  },
  error => {
    toast.hide();
    toast.show({text: '网络信号差，请稍后重试...', duration: 1000});
    return Promise.reject(error)
  }
)

const request = function({url = '', method = 'get', data = {}}) {
  let timestamp = new Date().getTime();
  data = JSON.stringify(data);
  if (url.indexOf("?") > 0) {
    url = url + `&body=${data}&jsonp=&appid=jd_healthy_wx&loginType=2&timestamp=${timestamp}`;
  } else {
    url = url + `?body=${data}&jsonp=&appid=jd_healthy_wx&loginType=2&timestamp=${timestamp}`;
  }

  return service({
    url,
    method
  })
}

export default request