function getIpAddress() {
  var RTCPeerConnection = window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
  var result = "";
  if (RTCPeerConnection) {
    (function() {
      var rtc = new RTCPeerConnection({ iceServers:[] });
      var addrs = Object.create(null);
      addrs["0.0.0.0"] = false;
      function grepSDP(sdp) {
        sdp.split("\r\n").forEach(function(line) {
          if (~line.indexOf("a=candidate")) {
            var parts = line.split(" "), addr = parts[4], type = parts[7];
            if (type === "host"){
              updateDisplay(addr);
            }
          } else if (~line.indexOf("c=")) {
            var parts = line.split(" "), addr = parts[2];
            updateDisplay(addr);
          }
        });
      }
      if (1 || window.mozRTCPeerConnection) {
        rtc.createDataChannel("", { reliable: false });
      }
      rtc.onicecandidate = function(evt) {
        if (evt.candidate) {
          grepSDP("a=" + evt.candidate.candidate);
        }
      };
      rtc.createOffer(function(offerDesc) {
        grepSDP(offerDesc.sdp);
        rtc.setLocalDescription(offerDesc);
      }, function(e) {
        console.warn("offer failed", e);
      });
      function updateDisplay(newAddr) {
        if (newAddr in addrs) return; else addrs[newAddr] = true;
        var displayAddrs = Object.keys(addrs).filter(function(k) {
          return addrs[k];
        });
        result = String(displayAddrs);
      }
    })()
  } else {
    result = "";
  }
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(result);
    }, 350)
  });
}

// 获取设备信息
export function getDeviceInfo() {
  let userAgentInfo = navigator.userAgent;
  let result = {};
  result.screenWidth   = window.screen.width;
  result.screenHeight  = window.screen.height;
  result.screenRatio   = window.devicePixelRatio;
  getIpAddress().then(res => { result.ipAddress = res });

  if (userAgentInfo.indexOf("iPhone") > -1) {
    result.osName = "ios";
    result.osVersion = userAgentInfo.replace(/^.*OS ([\d_]+) like.*$/, '$1').replace(/_/g, '.')
  } else if (userAgentInfo.indexOf("Android") > -1) {
    result.osName = "android";
    result.osVersion = userAgentInfo.replace(/^.*Android ([\d.]+);.*$/, '$1')
  } else {
    result.osName = "";
    result.osVersion = "";
  }

  var canvas = document.createElement('canvas');
  var gl = canvas.getContext('webgl');
  var debugInfo = gl.getExtension('WEBGL_debug_renderer_info');
  
  result.GPU = gl.getParameter(debugInfo.UNMASKED_RENDERER_WEBGL);
  result.WEBGL = gl.getParameter(gl.VERSION);


  return new Promise(resolve => {
    setTimeout(() => {
      resolve(result);
    }, 400)
  });
}
