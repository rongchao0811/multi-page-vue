window.__breezeScrollTop = 0;

const warn = console.warn;

function _preventDefault(e) {
	e.preventDefault();
}

/**
 * 禁止body滚动
 */
export const prohibitBodyScroll = function(e) {
	// document.addEventListener("touchmove", _preventDefault, {passive: false});
	let elementBody = document.getElementsByTagName('body')[0];
	window.__breezeScrollTop = document.documentElement.scrollTop || document.body.scrollTop;
	elementBody.style.cssText = `position:fixed;top:${-window.__breezeScrollTop}px;overflow:hidden;`;
}

/**
 * 恢复body滚动
 */
export const recoverBodyScroll = function(e) {
	// document.removeEventListener("touchmove", _preventDefault, {passive: false});
	let elementBody = document.getElementsByTagName('body')[0];
	elementBody.style.cssText = `position:static;top:auto;overflow:visible;`;
	document.documentElement.scrollTop = window.__breezeScrollTop;
}

/**
 * 判断是否为android机型
 */
export const isAndroid = function() {
	return navigator.userAgent.toLowerCase().indexOf('android') > -1
}

/**
 * 判断是否为空对象
 */
export const isEmptyObject = function(obj) {
	return JSON.stringify(obj) === '{}'
}

/**
 * 日期倒计时
 */
export const countDate = function(endTime) {
	var d,h,m,s;
	let startTime = new Date().getTime();
	let diffTime = endTime - startTime;

	if (diffTime >= 0) {
			d = Math.floor(diffTime/1000/60/60/24);
			h = Math.floor(diffTime/1000/60/60%24);
			m = Math.floor(diffTime/1000/60%60);
			s = Math.floor(diffTime/1000%60);
	}
	
	if (d > 0) {
		h = h + d * 24;
	}
	return h.toString().padStart(2, '0') + ':' + m.toString().padStart(2, '0') + ':' + s.toString().padStart(2, '0');
}

/**
 * 获取当前页面url参数
 */
export const getCurrentUrlSearch = function() {
	let search = window.location.search;
	let arr = [];
	let result = {};
	if (!search) {
		return ''
	}
	search = search.substring(1);
	arr = search.split('&');
	for (let i = 0; i < arr.length; i++) {
		let pos = arr[i].indexOf('=');
		if(pos == -1) {
			continue;
		}
		let name = arr[i].substring(0, pos);
		let value = arr[i].substring(pos + 1);
		value = decodeURIComponent(value);
		result[name] = value;
	}
	return result;
}

/**
 * 数字前缀补“0”
 */
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 * 格式化日期
 * 默认格式：YYYY-MM-DD
 */
export const formatDate = function(date, separator, isTime) {
	date         = new Date(date);
	const year   = date.getFullYear();
  const month  = date.getMonth() + 1
  const day    = date.getDate();
  const hour   = date.getHours();
  const minute = date.getMinutes();
	const second = date.getSeconds();

	const numberToDecimal = function(item) {
		if (item == null) {
			warn(`numberToDecimal: The item is ${item}`);
			item = '';
		}
		return String(item).padStart(2, '0')
	};

	let dates = [ year, month, day ].map(numberToDecimal);
	let times = [ hour, minute, second ].map(numberToDecimal);

	if (isTime) {
		return dates.join(separator) + ' ' + times.join(':');
	} else {
		return dates.join(separator);
	}
}

// 微信小程序内嵌H5
export const postMinigramMessage = function(params = {}) {
	return new Promise(resolve => {
		wx.miniProgram.getEnv(function(res) {
			if (res.miniprogram) {
				wx.miniProgram.postMessage({data: params});
				resolve(res)
			}
		});
	});
}

/**
 * APP内嵌H5分享功能
 * 安卓和ios不同的写法
 */
function __connectWebViewJavascriptBridge(callback) {
	if (window.WebViewJavascriptBridge) {
		callback(WebViewJavascriptBridge)
	} else {
		document.addEventListener('WebViewJavascriptBridgeReady', function() {
			callback(WebViewJavascriptBridge)
		}, false);
	}
}
function __setupWebViewJavascriptBridge(callback) {
	if (window.WebViewJavascriptBridge) {
		return callback(WebViewJavascriptBridge);
	}
	if (window.WVJBCallbacks) {
		return window.WVJBCallbacks.push(callback);
	}
	window.WVJBCallbacks = [callback];
	let WVJBIframe = document.createElement('iframe');
	WVJBIframe.style.display = 'none';
	WVJBIframe.src = 'https://__bridge_loaded__';
	document.documentElement.appendChild(WVJBIframe);
	setTimeout(() => {
		document.documentElement.removeChild(WVJBIframe);
	}, 0);
}
export const appShareH5 = function(params = {}) {

	let shareData = {
		title: params.title,
		content: params.content,
		imgUrl: params.imgUrl,
		link: params.link,
		platformType: params.platformType
	};

	if (isAndroid()) {
		// android
		// 分享
		if (window.WebViewJavascriptBridge) {
			WebViewJavascriptBridge.callHandler('shareCurrentPage', shareData, function(response) {});
		}
		// 分享成功
		__connectWebViewJavascriptBridge(function(bridge) {
			bridge.init(function(message, responseCallback) {});
			bridge.registerHandler('router://shareComplete', function(data) {
				if (data.code == 0) {
					params.successCallback && params.successCallback();
				} else {
					warn(`app android share h5 page fail`);
					params.errorCallback && params.errorCallback();
				}
			});
		});

	} else {
		// ios
		// 分享成功
		__setupWebViewJavascriptBridge(function(bridge) {
			bridge.registerHandler('shareComplete', function(data) {
				if (data.code == 0) {
					params.successCallback && params.successCallback();
				} else {
					warn(`app ios share h5 page fail`);
					params.errorCallback && params.errorCallback();
				}
			});
			// 分享
			bridge.callHandler('router://shareCurrentPage', shareData, function(response) {});
		});
	}

}

// 对象序列化
export const jsonSerialize = data => {
  var str = '';
  for (var key in data) {
    str += key + '=' + encodeURIComponent(data[key]) + '&';
  }
  return str.substring(0, str.length - 1);
}
// URL地址拼接
export const setUrl = function(data) {
	let url = window.location.href;
	let search = jsonSerialize(data);

	return url.indexOf('?') > -1 ? `${url}&${search}` : `${url}?${search}`;
}

/**
 * 京东统一登录
 */

 export const jdLogin = function() {
	window.location.href=`https://plogin.m.jd.com/user/login.action?appid=jd_healthy_wx&returnurl=${encodeURIComponent(window.location.href.toString())}`;
 }