
// 获取url参数
export function getUrlParams(url) {
  let result = {};
  let query = url ? url.split('?')[1] : window.location.search.slice(1);
  
  if (!query) { return result; }
  query = query.split('#')[0];

  let arr = query.split('&');

  for(let i = 0; i < arr.length; i++) {
    let data = arr[i].split('=');
    let itemName = data[0];
    let itemValue = typeof(data[1]) === 'undefined' ? 'undefined' : data[1];

    if (itemName.match(/\[(\d+)?\]$/)) {
      let key = itemName.replace(/\[(\d+)?\]/, '');

      if (!result[key]) { result[key] = [] };
      if (itemName.match(/\[(\d+)\]$/)) {
        let index = /\[(\d+)\]/.exec(itemName)[1];
        result[key][index] = itemValue;
      } else {
        result[key].push(itemValue);
      }
    } else {
      if (!result[itemName]) {
          result[itemName] = itemValue;
      } else if (result[itemName] && typeof result[itemName] === 'string') {
          result[itemName] = [result[itemName]];
          result[itemName].push(itemValue);
      } else {
          // 如果是其它的类型，还是往数组里丢
          result[itemName].push(itemValue);
      }
    }
  }

  return result;

}

export function isWeiXin() {
  return /micromessenger/i.test(navigator.userAgent.toLowerCase()) || typeof navigator.wxuserAgent !== 'undefined'
}
export function judgePhoneType() {
  let isAndroid = false, isIOS = false, isIOS9 = false, version,
      u = navigator.userAgent,
      ua = u.toLowerCase();
  //Android系统
  if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {   //android终端或者uc浏览器
      isAndroid = true
  }
  //ios
  if (ua.indexOf("like mac os x") > 0) {
      let regStr_saf = /os [\d._]*/gi;
      let verinfo = ua.match(regStr_saf);
      version = (verinfo + "").replace(/[^0-9|_.]/ig, "").replace(/_/ig, ".");
  }
  let version_str = version + "";
  // ios9以上
  if (version_str !== "undefined" && version_str.length > 0) {
      version = parseInt(version);
      if (version >= 8) {
          isIOS9 = true
      } else {
          isIOS = true
      }
  }
  return {isAndroid, isIOS, isIOS9};
}

// 打开APP
export function openApp(url, callback) {
  let {isAndroid, isIOS, isIOS9} = judgePhoneType();

   if (!isIOS9) {
     let hasApp = true;
     let t = 1000;
     let t1 = Date.now();
     let ifr = document.createElement("iframe");

     ifr.setAttribute('src', url);
     ifr.setAttribute('style', 'display:none');
     document.body.appendChild(ifr);

     setTimeout(function() {
       if (!hasApp) {
         callback && callback()
       }
       document.body.removeChild(ifr);
     }, 1800);

     setTimeout(function () { //启动app时间较长处理
       let t2 = Date.now();
       if (t2 - t1 < t + 100) {
         hasApp = false;
       }
     }, t);
   } else {
     location.href = url;
     setTimeout(function () {
       callback && callback()
     }, 250);
     setTimeout(function () {
       location.reload();
     }, 1000);
   }
}