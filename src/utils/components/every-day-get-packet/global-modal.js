import ModalButtonEnsure from "@/components/every-day-get-packet/Modal/common/button-ensure"
import ModalButtonClose from "@/components/every-day-get-packet/Modal/common/button-close"
import ModalTopTitle from "@/components/every-day-get-packet/Modal/common/top-title"

function plugin(Vue) {
	if (plugin.installed) {
		return
	}
	Vue.component("glModalButtonEnsure", ModalButtonEnsure);
	Vue.component("glModalButtonClose", ModalButtonClose);
	Vue.component("glModalTopTitle", ModalTopTitle);
}

export default plugin