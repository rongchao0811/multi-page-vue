import price from "@/components/every-day-get-packet/common/price"

function plugin(Vue) {
	if (plugin.installed) {
		return
	}
	Vue.component("glPrice", price);
}

export default plugin