import preLoad from "@/components/common/pre-load/load-img.vue"
import scrollSingle from '@/components/common/scroll-single/index.vue'
import circle from '@/components/common/circle/index.vue'

function plugin(Vue) {
	if (plugin.installed) {
		return
	}
	Vue.component("glPreLoadImg", preLoad);
	Vue.component("glScrollSingle", scrollSingle);
	Vue.component("glCircle", circle);
}

export default plugin